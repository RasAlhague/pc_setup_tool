pc_setup_tool:
==============
Funktions:
----------
 - Installs all Standard Programms
 - Installs all drivers
 - installs update
 - Clone Repositories
 - Clone UserData onto new system
 
Lang:
-----
 - Rust, Batch, Powershell
 
Working order installation:
===========================
 - install update
	-> installs windows updates and restarts the pc
 - install driver
	-> installs driver updates and restarts the pc
 - install standard programms
	-> installs standard programms and if a standard programm requires restart restards and continues with installing
 - clone repositories
	-> clones all repositories that are specified
 - clone userdata
	-> clones the userdata and moves it to the wished coordinates on pc
 - runs another time windows updates to ensure every programm is updated
 
Programm Aufbau:
================
 - Gesplittet in hauptprogramme und unterprogramme
 - Hauptprogramm startet nach und nach die unterprogramme und sammelt alle fortschrittsinformationen
 - Unterprogramme machen die spezialisierten workloads
 
Programme:
==========

pc_settup_runner:
-----------------
 - starts the subprograms
 - holds track of the setup process
 - organizes logs of finished programms
 - displays the overall process
 
pc_win_updater:
-----------
 - runs an windows update and restarts the pc when updated
 - redos until no updates are found
 
pc_driver_updater:
------------------
 - identifies hardware
 - searches for drives for hardware
 - runs driver updates 
 - restart pc after driver updates

pc_program_installer: 
---------------------
 - read the installation instructions file 
 - installs the programs listed in the installation settings file to the given location with given parameters
 - saves progress if programm is installed, and needs restart of pc
 - continues this process until everything is installed

pc_repo_cloner:
---------------
 - reads a config file for git repositories
 - clones the repos according to the config file

pc_userdata_cloner:
-------------------
 - reads a config file for git repositories
 - clones the userdata according to the config file
 - moves the userdata according to its sub config files
 
Wichtige Programme:
-------------------
Entwicklung:
 - Notepad++
 - VS 2019 Community
 - VS Code, RLS
 - RustUp/Cargo (alle tools für die entwicklung von allem)
 - Xampp
 - Unity
 - Git
 - GitKraken
 - markdown editor (muss noch einen finden)
 - tools für rust embedded
Spiele:
 - Steam
 - Origin
 - Uplay
 - Xbox Gameclient
 - Battle.Net
Browser:
 - Opera GX
 - Chrome
 - Firefox
Sonstiges:
 - VLC
 - WinRar
 - Avast